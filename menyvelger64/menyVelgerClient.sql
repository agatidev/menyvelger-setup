
	USE master;
	GO

	IF EXISTS(SELECT * FROM sys.databases where name = 'menyVelgerClient')
	DROP DATABASE menyVelgerClient;
	GO
	Create Database menyVelgerClient;
	GO
	
	USE menyVelgerClient;
	GO	
	
	Create Table tblAddon(
	addonID bigint primary key,
	restaurantID bigint,
	addonName nvarchar(max),
	addonTypeName1 nvarchar(max),
	addonTypePrice1 int,
	addonTypeName2	nvarchar(max),
	addonTypePrice2 int,
	addonTypeName3 nvarchar(max),
	addonTypePrice3 int,
	publish bit,
	displayOrder int,
	addonDateUpdated datetime
	);
	GO
	
	
	Create Table tblAddSubAddon(
	addSubAddonID bigint primary key,
	addonID bigint,
	subCategoryID bigint,
	addSubAddonDateUpdated datetime
	);
	GO
	
	
	Create Table tblCategory(
	categoryID bigint primary key,
	categoryName nvarchar(max),
	categoryImage varbinary(max),
	restaurantID	bigint,
	publish bit,
	displayOrder int,
	categoryDateUpdated datetime
	);
	GO
	
	
	Create Table tblRestaurant(
	restaurantID bigint primary key,
	restaurantName nvarchar(max),
	restaurantLogo varbinary(max),
	restaurantUserName nvarchar(max),
	restaurantUserPassword	varbinary(max),
	restaurantAddress nvarchar(max),
	publish bit,
	idleTime int,
	dbRefreshTime int,
	restaurantDateUpdated datetime
	);
	GO
	
	
	Create Table tblScreenSaver(
	screenSaverID bigint primary key,
	ssCaption nvarchar(max),
	ssImage varbinary(max),
	ssInterval int,
	ssRestID bigint,
	publish bit,
	displayOrder int,
	screenSaverDateUpdated datetime
	);
	GO
	
	
	
	Create Table tblSubCategory(
	subCategoryID bigint primary key,
	categoryID bigint,
	subCategoryName nvarchar(max),
	subCategoryImage varbinary(max),
	subCategoryDescription nvarchar(max),
	subCategoryPrice	int,
	publish bit,
	restaurantID bigint,
	displayOrder int,
	subCategoryDateUpdated datetime
	);
	GO
	
	
	create table usertblAddon
	  (
	  userAddonID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	  restaurantID VARCHAR(100),
	  addonID VARCHAR(100),
	  subCategoryID VARCHAR(100),
	  addonType VARCHAR(100) NULL ,
	  addonTypeClick VARCHAR(100) NULL,
	  );
	  GO
  
  
	Create Table userAddonLog(
	activityID bigint primary key identity(1,1),
	orderNumber int,
	transactionID nvarchar(max),
	restaurantID bigint,
	subCategoryID bigint,
	addonID bigint,
	addonName nvarchar(max),
	addonType nvarchar(max),
	quantity int,
	flag nvarchar(max),
	capturedTime datetime default current_timestamp,
	addonPrice nvarchar(max)
	);
	GO
	
	
	Create Table userActivityLog(
	activityID bigint primary key identity(1,1),
	orderNumber int,
	transactionID nvarchar(max),
	restaurantID bigint,
	categoryID bigint,
	categoryName nvarchar(max),
	subCategoryID bigint,
	subCategoryName nvarchar(max),
	statusMessage nvarchar(max),
	statusString	nvarchar(max),
	capturedTime datetime default current_timestamp,
	flag nvarchar(max),
	quantity int,
	subCategoryPrice nvarchar(max)
	);
	GO
	
	 create table usertblSubCategory
	  (
	  userSubCategoryID BIGINT NOT NULL IDENTITY(1,1) PRIMARY KEY,
	  restaurantID VARCHAR(100),
	  subCategoryID VARCHAR(100),
	  categoryID BIGINT NOT NULL,
	  quantity int
	  );
	  GO
	  
  
  
  CREATE TABLE configTable (
    parameterID    BIGINT IDENTITY (1, 1) NOT NULL,
    parameterName NVARCHAR(MAX) NOT NULL,
    parameterValue NVARCHAR(MAX) NOT NULL,
    PRIMARY KEY CLUSTERED (parameterID ASC)
	);
	GO

	insert into configTable values ('transactionCount','1');
	GO
	insert into configTable values ('dbSyncTime',CURRENT_TIMESTAMP);
	GO
	insert into configTable values ('restaurantID',0);
	GO



CREATE PROCEDURE createAddSubAddonTable
AS
BEGIN
	Create Table tblAddSubAddon(
	addSubAddonID bigint primary key,
	addonID bigint,
	subCategoryID bigint,
	addSubAddonDateUpdated datetime
	)
END

GO
	

CREATE PROCEDURE checkRestaurantToContinue
	@restaurantID bigint,
	@restUserName nvarchar(max),
	@restPassword nvarchar(max)
AS
BEGIN
	IF NOT EXISTS(SELECT * FROM dbo.tblRestaurant where restaurantUserName = @restUserName and publish = 1
	and convert(nvarchar(max),DECRYPTBYPASSPHRASE('key',restaurantUserPassword))= @restPassword)
		BEGIN
			SELECT *, 'Password Changed' AS MESSAGE FROM dbo.tblRestaurant where restaurantID = @restaurantID;
		END
	ELSE			
		SELECT *,'Password Not Changed' AS MESSAGE FROM dbo.tblRestaurant where restaurantUserName = @restUserName and publish = 1
		and convert(nvarchar(max),DECRYPTBYPASSPHRASE('key',restaurantUserPassword))= @restPassword;
END
GO



CREATE PROCEDURE deleteExtra
    @subCategoryID nvarchar(50),
    @addonID nvarchar(50),
    @restaurantID nvarchar(50),
    @addonType nvarchar(max)
AS 
BEGIN
	if((select addonTypeClick from dbo.usertblAddon where restaurantID = @restaurantID and subCategoryID = @subCategoryID
	and addonID = @addonID and addonType = @addonType)>1)
	begin
	update dbo.usertblAddon set addonTypeClick = addonTypeClick - 1 where restaurantID = @restaurantID 
	and subCategoryID = @subCategoryID and addonID = @addonID and addonType = @addonType;
	 end
	 else
	 begin
	delete from dbo.usertblAddon where restaurantID = @restaurantID 
	and subCategoryID = @subCategoryID and addonID = @addonID and addonType = @addonType;
	end
END
GO

	
CREATE PROCEDURE [dbo].[getCategoryByIndex]
	@restaurantID int,
	@index int
AS
BEGIN
	SELECT * FROM(SELECT *, row_number() OVER (ORDER BY displayOrder,categoryName) AS rownum FROM dbo.tblCategory 
	where restaurantID = @restaurantID and publish =  1) AS A WHERE A.rownum > @index;
END
GO	
	
	
	CREATE PROCEDURE [dbo].[getCategoryByID]
	@categoryID int
AS
BEGIN
	SELECT * FROM dbo.tblCategory where categoryID = @categoryID;
END
GO


CREATE PROCEDURE [dbo].[getAllSubCategory]
AS
BEGIN
	SELECT * from dbo.tblSubCategory;
END
GO


CREATE PROCEDURE [dbo].[getAllRestaurants]
AS
BEGIN
	IF OBJECT_ID('dbo.tblRestaurant', 'U') IS NOT NULL
	SELECT * from dbo.tblRestaurant;
END
GO


CREATE PROCEDURE [dbo].[getAddonSelected]
	@restaurantID int,
	@subCategoryID int
AS
BEGIN
	SELECT * FROM dbo.usertblAddon uan LEFT JOIN dbo.tblAddon an ON uan.addonID = an.addonID 
	where uan.restaurantID = @restaurantID and uan.subCategoryID = @subCategoryID;
END
GO
	
	CREATE PROCEDURE [dbo].[getAddonLogData]
	@orderNumber nvarchar(max),
	@transactionID nvarchar(max),
	@subCategoryID nvarchar(max)
AS
BEGIN
	SELECT * from dbo.userAddonLog where orderNumber = @orderNumber and transactionID = @transactionID
	and subCategoryID = @subCategoryID;
END
GO


create procedure dbo.getAddonBySubCategoryID
@subCategoryID bigint
as 
select addsub.*, addon.* from dbo.tblAddSubAddon addsub
LEFT JOIN dbo.tblAddon addon on addsub.addonID = addon.addonID
where addsub.subCategoryID = @subCategoryID and addon.publish = 1;
GO

CREATE PROCEDURE [dbo].[getAddonByIndex]
	@subCategoryID int,
	@index int
AS	
BEGIN
	SELECT * FROM
(
    SELECT s.addSubAddonID, s.subCategoryID, addon.addonID,addon.restaurantID,addon.addonName,
    addon.addonTypeName1,addon.addonTypeName2,addon.addonTypeName3,addon.addonTypePrice1,
    addon.addonTypePrice2,addon.addonTypePrice3,addon.displayOrder,addon.publish ,ROW_NUMBER()
     OVER (ORDER BY addon.displayOrder,addon.addonName) AS rownum FROM dbo.tblAddon addon
     LEFT JOIN dbo.tblAddSubAddon s
     ON s.addonID = addon.addonID
     WHERE s.subCategoryID = @subCategoryID and addon.publish = 1
) as addonDetail WHERE addonDetail.rownum > @index;
END
GO


CREATE PROCEDURE [dbo].[getAddonByID]
	@addonID int
AS
BEGIN
	select * from dbo.tblAddon where addonID = @addonID;
END
GO
	
	
	CREATE PROCEDURE [dbo].[dropAddSubAddon]
AS
BEGIN
	IF OBJECT_ID('dbo.tblAddSubAddon', 'U') IS NOT NULL
	drop table dbo.tblAddSubAddon;
	exec dbo.createAddSubaddonTable;
END
GO


CREATE PROCEDURE [dbo].[deleteUserSelections ]
		@restaurantID nvarchar(50)
AS 
BEGIN

	delete from dbo.usertblAddon where restaurantID = @restaurantID;
	delete from dbo.usertblSubCategory where restaurantID = @restaurantID;
END
GO	
	
	
	CREATE PROCEDURE [dbo].[deleteSubCategory ]
    @subCategoryID nvarchar(50),
    @restaurantID nvarchar(50)
AS 
BEGIN
	if((select quantity from dbo.usertblSubCategory where restaurantID = @restaurantID and subCategoryID = @subCategoryID)>1)
	begin
	update dbo.usertblSubCategory set quantity = quantity - 1 where restaurantID = @restaurantID 
	and subCategoryID = @subCategoryID;
	 end
	 else
	 begin
	delete from dbo.usertblAddon where restaurantID = @restaurantID and subCategoryID = @subCategoryID;
	delete from dbo.usertblSubCategory where restaurantID = @restaurantID and subCategoryID = @subCategoryID;
	end
	END
	GO
	
	CREATE PROCEDURE [dbo].[deleteLogData]
	@logStatus nvarchar(max)
AS
BEGIN
	IF (@logStatus='addon')
	delete from dbo.userAddonLog where flag='R';
	ELSE
	delete from dbo.userActivityLog where flag='R';
END
GO



CREATE PROCEDURE [dbo].[userAuthenticate]
	@userName nvarchar(max),
	@password nvarchar(max)
AS
BEGIN
DECLARE @restID BIGINT;
	IF exists(SELECT * from dbo.tblRestaurant where restaurantUserName = @userName and convert(nvarchar(max),DecryptByPassPhrase('key', restaurantUserPassword)) = @password)
	begin
	SET @restID = (select restaurantID from dbo.tblRestaurant where restaurantUserName = @userName 
	and convert(nvarchar(max),DecryptByPassPhrase('key', restaurantUserPassword)) = @password);
	
	SELECT *, 'Authenticated' as Message from dbo.tblRestaurant 
	where restaurantID = @restID;
	UPDATE dbo.configTable SET parameterValue = @restID WHERE parameterName = 'restaurantID';
	DELETE FROM dbo.tblRestaurant where restaurantID != @restID;
	end
	else
	begin
	select 'Not Authenticated' as Message;
	end
	END
	GO
	
	create procedure dbo.updateSyncTime
@syncTime dateTime
as 
BEGIN
UPDATE dbo.configTable set parameterValue = @syncTime 
where parameterName = 'dbSyncTime';
END
GO

create procedure dbo.updateRestaurantPassword
	@restaurantID bigint,
	@restUserName nvarchar(max),
	@restPassword nvarchar(max)
AS
BEGIN
	UPDATE dbo.tblRestaurant SET restaurantUserName = @restUserName, restaurantUserPassword = ENCRYPTBYPASSPHRASE('key',@restPassword)
				where restaurantID = @restaurantID;
END
GO

create procedure dbo.updateRestaurantByID
	@restaurantID bigint,
	@restaurantLogo varbinary(max),
	@restaurantAddress nvarchar(max),
	@idleTime bigint,
	@dbIntervalTime bigint,
	@publish bit
AS
BEGIN
	update tblRestaurant set restaurantLogo = @restaurantLogo, restaurantAddress = @restaurantAddress,
	idleTime = @idleTime, dbRefreshTime = @dbIntervalTime,publish=@publish where restaurantID = @restaurantID;
	
	END
	GO
	
	CREATE PROCEDURE [dbo].[updateLogData]
	@logStatus nvarchar(max),
	@activityID nvarchar(max)
AS
BEGIN
	IF (@logStatus='addon')
	begin
	update dbo.userAddonLog set flag='R'
	where activityID = @activityID;
	end
	ELSE
	begin
	update dbo.userActivityLog set flag='R'
	where activityID = @activityID;
	end
END
GO


CREATE PROCEDURE [dbo].[insertUserSubCategory ]
	@categoryID nvarchar(50),
    @subCategoryID nvarchar(50),
    @restaurantID nvarchar(50)
AS 
BEGIN
	if not exists(select * from dbo.usertblSubCategory where restaurantID = @restaurantID and subCategoryID = @subCategoryID and categoryID = @categoryID)
	begin
		insert into dbo.usertblSubCategory values (@restaurantID, @subCategoryID,@categoryID,1);
	end
	else
	begin
		update dbo.usertblSubCategory set quantity = quantity + 1 
		where restaurantID = @restaurantID and subCategoryID = @subCategoryID and categoryID = @categoryID;
		end
	END
	GO	
		
	CREATE PROCEDURE dbo.insertUserAddons
    @addonID nvarchar(50), 
    @subCategoryID nvarchar(50), 
    @addonType nvarchar(50),
    @restaurantID nvarchar(50)
	AS 
	BEGIN
	if exists(select * from dbo.usertblSubCategory where restaurantID = @restaurantID and subCategoryID = @subCategoryID)
	begin
	  if exists (select * from  dbo.usertblAddon where restaurantID = @restaurantID and addonID = @addonID and subCategoryID = @subCategoryID)
	 begin
	 update dbo.usertblAddon set addonTypeClick = 1 , addonType = @addonType 	 
	where addonID = @addonID and subCategoryID = @subCategoryID
	end
	else
	begin
	insert into dbo.usertblAddon values (@restaurantID, @addonID,@subCategoryID,@addonType, 1);	
	end
	end
	END
	GO
	
	CREATE PROCEDURE [dbo].[insertUserAddonLog]
	@orderNumber int,
	@transactionID nvarchar(max),
	@restaurantID bigint,
	@subCategoryID bigint,
	@addonID bigint,
	@addonName nvarchar(max),
	@addonType nvarchar(max),
	@quantity int,
	@addonPrice nvarchar(max)
	AS
	BEGIN
	INSERT INTO dbo.userAddonLog (orderNumber, transactionID, restaurantID, subCategoryID, addonID,addonName, addonType,quantity, flag,addonPrice) VALUES (@orderNumber, @transactionID,@restaurantID,@subCategoryID,@addonID,@addonName,@addonType,@quantity,'NR',@addonPrice);
	END
	GO


CREATE PROCEDURE [dbo].[insertSubCategory]
	@subCategoryID bigint,
	@categoryID bigint,
	@subCategoryName nvarchar(max),
	@subCategoryImage varbinary(max),
	@subCategoryDescription nvarchar(max),
	@subCategoryPrice int,
	@publish bit,
	@restaurantID bigint,
	@displayOrder int,
	@subCategoryDateUpdated datetime,
	@statusFlag nvarchar(max)
AS
BEGIN
IF @statusFlag = 'insert'
begin
	insert into dbo.tblSubCategory values(@subCategoryID,@categoryID,@subCategoryName,@subCategoryImage,@subCategoryDescription,
	@subCategoryPrice,@publish,@restaurantID,@displayOrder,@subCategoryDateUpdated);
	end
	ELSE
	begin
	IF Exists (select * from dbo.tblSubCategory where subCategoryID = @subCategoryID)
	update dbo.tblSubCategory set categoryID = @categoryID, subCategoryName = @subCategoryName,subCategoryImage=@subCategoryImage,
	subCategoryDescription=@subCategoryDescription,subCategoryPrice=@subCategoryPrice,publish=@publish,restaurantID=@restaurantID,
	displayOrder=@displayOrder,subCategoryDateUpdated=@subCategoryDateUpdated where subCategoryID=@subCategoryID;
	ELSE
	insert into dbo.tblSubCategory values(@subCategoryID,@categoryID,@subCategoryName,@subCategoryImage,@subCategoryDescription,
	@subCategoryPrice,@publish,@restaurantID,@displayOrder,@subCategoryDateUpdated);
	end
END
GO


CREATE PROCEDURE [dbo].[insertScreenSaver]
	@screenSaverID bigint,
	@ssCaption nvarchar(max),
	@ssImage varbinary(max),
	@ssInterval int,
	@ssRestID bigint,
	@publish bit,
	@displayOrder int,
	@screenSaverDateUpdated datetime,
	@statusFlag nvarchar(max)
AS
BEGIN
IF @statusFlag = 'insert'
begin
	insert into dbo.tblScreenSaver values(@screenSaverID,@ssCaption,@ssImage,@ssInterval,@ssRestID,@publish,@displayOrder,@screenSaverDateUpdated);
	end
	ELSE
	begin
	IF Exists (select * from dbo.tblScreenSaver where screenSaverID = @screenSaverID)
	update dbo.tblScreenSaver set screenSaverID = @screenSaverID, ssCaption = @ssCaption,ssImage=@ssImage,
	ssInterval=@ssInterval,ssRestID=@ssRestID,publish=@publish,
	displayOrder=@displayOrder,screenSaverDateUpdated=@screenSaverDateUpdated where screenSaverID=@screenSaverID;
	ELSE
	insert into dbo.tblScreenSaver values(@screenSaverID,@ssCaption,@ssImage,@ssInterval,@ssRestID,@publish,@displayOrder,@screenSaverDateUpdated);
	end
END
GO


CREATE PROCEDURE [dbo].[insertRestaurant]
	@restaurantID bigint,
	@restaurantName nvarchar(max),
	@restaurantUserName nvarchar(max),
	@restaurantUserPassword	varbinary(max),
	@restaurantDateUpdated datetime
AS
BEGIN
	insert into dbo.tblRestaurant 
	(restaurantID,restaurantName,restaurantUserName,restaurantUserPassword,restaurantDateUpdated) values
	(@restaurantID,@restaurantName,@restaurantUserName,@restaurantUserPassword,@restaurantDateUpdated);
END
GO


CREATE PROCEDURE [dbo].[insertCategory]
	@categoryID bigint,
	@categoryName nvarchar(max),
	@categoryImage varbinary(max),
	@restaurantID	bigint,
	@publish bit,
	@displayOrder int,
	@categoryDateUpdated datetime,
	@statusFlag nvarchar(max)
AS
BEGIN
IF @statusFlag = 'insert'
BEGIN
	insert into dbo.tblCategory values(@categoryID,@categoryName,@categoryImage,@restaurantID,@publish,@displayOrder,@categoryDateUpdated);
	END
	ELSE
	BEGIN
	IF EXISTS (SELECT * FROM dbo.tblCategory where categoryID = @categoryID)
	UPDATE dbo.tblCategory SET categoryName = @categoryName,categoryImage=@categoryImage,restaurantID=@restaurantID,publish=@publish,
	displayOrder=@displayOrder,categoryDateUpdated=@categoryDateUpdated where categoryID = @categoryID;
	ELSE
	insert into dbo.tblCategory values(@categoryID,@categoryName,@categoryImage,@restaurantID,@publish,@displayOrder,@categoryDateUpdated);
	END
END
GO


CREATE PROCEDURE [dbo].[insertAddSubAddon]
	@addSubAddonID bigint,
	@addonID bigint,
	@subCategoryID bigint,
	@addSubAddonDateUpdated datetime
AS
BEGIN
	insert into dbo.tblAddSubAddon values(@addSubAddonID,@addonID,@subCategoryID,@addSubAddonDateUpdated);
END
GO


CREATE PROCEDURE [dbo].[insertAddon]
	@addonID bigint,
	@restaurantID bigint,
	@addonName nvarchar(max),
	@addonTypeName1 nvarchar(max),
	@addonTypePrice1 nvarchar(max),
	@addonTypeName2 nvarchar(max),
	@addonTypePrice2 nvarchar(max),
	@addonTypeName3 nvarchar(max),
	@addonTypePrice3 nvarchar(max),
	@publish bit,
	@displayOrder int,
	@addonDateUpdated datetime,
	@statusFlag nvarchar(max)
AS
	BEGIN
	IF @statusFlag = 'insert'
BEGIN
	insert into dbo.tblAddon values(@addonID,@restaurantID,@addonName,@addonTypeName1,@addonTypePrice1,@addonTypeName2,@addonTypePrice2,
	@addonTypeName3,@addonTypePrice3,@publish,@displayOrder,@addonDateUpdated);
	END
	ELSE
	BEGIN
	IF EXISTS (SELECT * FROM dbo.tblAddon where addonID = @addonID)
	UPDATE dbo.tblAddon SET restaurantID=@restaurantID,addonName=@addonName,addonTypeName1=@addonTypeName1,
	addonTypePrice1=@addonTypePrice1,addonTypeName2=@addonTypeName2,addonTypePrice2=@addonTypePrice2,addonTypeName3=@addonTypeName3,
	addonTypePrice3=@addonTypePrice3,publish=@publish,
	displayOrder=@displayOrder,addonDateUpdated=@addonDateUpdated where addonID = @addonID;
	ELSE
	insert into dbo.tblAddon values(@addonID,@restaurantID,@addonName,@addonTypeName1,@addonTypePrice1,@addonTypeName2,@addonTypePrice2,
	@addonTypeName3,@addonTypePrice3,@publish,@displayOrder,@addonDateUpdated);
	END
END
GO

CREATE PROCEDURE [dbo].[insertUserActivity]
	@transactionID nvarchar(max),
	@restaurantID bigint,
	@categoryID bigint,
	@categoryName nvarchar(max),
	@subCategoryID bigint,
	@subCategoryName nvarchar(max),
	@quantity int,
	@statusMessage nvarchar(max),
	@statusString	nvarchar(max),
	@transactionStatus nvarchar(max),
	@subCategoryPrice nvarchar(max)
AS
BEGIN
DECLARE @transID nvarchar(max);
	IF (@transactionStatus = 'intermediate')
	BEGIN
			SET @transID = (SELECT parameterValue FROM dbo.configTable where parameterName = 'transactionCount');
			IF (@transID IS NULL)
			SET @transID = 1;
			SET @transactionID = @transactionID + '-'+@transID;
			UPDATE dbo.configTable SET parameterValue = parameterValue+1 where parameterName = 'transactionCount';
	END
	ELSE
	BEGIN
		SET @transID = (SELECT TOP 1 orderNumber FROM dbo.userActivityLog WHERE transactionID = @transactionID);
	END
	INSERT INTO dbo.userActivityLog (orderNumber, transactionID, restaurantID, categoryID, categoryName,subCategoryID, subCategoryName, quantity, statusMessage, statusString,flag,subCategoryPrice) VALUES (@transID, @transactionID,@restaurantID,@categoryID,@categoryName,@subCategoryID,@subCategoryName, @quantity, @statusMessage,@statusString,'NR',@subCategoryPrice);
	SELECT @transactionID AS TRANSACTIONID;
END
GO

CREATE PROCEDURE [dbo].[getUserSelection]
	@restaurantID int
AS
BEGIN
	SELECT usc.*, sc.*,c.* FROM dbo.usertblSubCategory usc LEFT JOIN dbo.tblSubCategory sc ON usc.subCategoryID = sc.subCategoryID 
	LEFT JOIN dbo.tblCategory c ON usc.categoryID = c.categoryID WHERE usc.restaurantID = @restaurantID;
END
GO
	
	CREATE PROCEDURE [dbo].[getSubCategoryByIndex]
	@categoryID int,
	@index int
AS
BEGIN
	SELECT * FROM (SELECT *, row_number() OVER (ORDER BY displayOrder,subCategoryName) AS rownum 
	FROM dbo.tblSubCategory where categoryID = @categoryID and publish = 1) AS A WHERE A.rownum > @index;
END
GO


CREATE PROCEDURE [dbo].[getSubCategoryByID]
	@subCategoryID int
AS
BEGIN
	SELECT * FROM dbo.tblSubCategory where subCategoryID =@subCategoryID
	END
GO
	
	CREATE PROCEDURE [dbo].[getSubCategoryByCategoryID]
	@categoryID int
AS
BEGIN
	SELECT * FROM dbo.tblSubCategory where categoryID = @categoryID
	and publish = 1;
	END
GO
	
	CREATE PROCEDURE [dbo].[getScreenSaverByIndex]
	@restaurantID int,
	@index int
AS
BEGIN
	SELECT * FROM(SELECT *, row_number() OVER (ORDER BY displayOrder) AS rownum FROM dbo.tblScreenSaver 
	where ssRestID = @restaurantID and publish = 1) AS A WHERE A.rownum > @index;
END
GO
	
	create procedure dbo.getRestaurantByID
@restaurantID bigint
as 
BEGIN
select * from dbo.tblRestaurant where restaurantID = @restaurantID;
END
GO


create procedure dbo.getLogSubCategoryByCategoryID
@categoryID bigint
as 
BEGIN
select distinct u.subCategoryName, u.subCategoryID,u.statusMessage,u.quantity,s.subCategoryPrice from dbo.userActivityLog u
join tblSubCategory s on s.subCategoryID = u.subCategoryID
where u.categoryID = @categoryID;
END
GO


create procedure dbo.getLogDataByCategory
as 
BEGIN
select distinct categoryName, categoryID from dbo.userActivityLog;
END
GO


CREATE PROCEDURE [dbo].[getLogData]
AS
BEGIN
	SELECT * from dbo.userActivityLog;
END
GO


CREATE PROCEDURE [dbo].[getConfigByName]
@paramName nvarchar(max)
AS
BEGIN
select * from dbo.configTable where parameterName = @paramName;
END
GO


CREATE PROCEDURE [dbo].[getCategoryByRestaurantID]
	@restaurantID int
AS
BEGIN
	SELECT * FROM dbo.tblCategory where restaurantID = @restaurantID;
	END
GO
